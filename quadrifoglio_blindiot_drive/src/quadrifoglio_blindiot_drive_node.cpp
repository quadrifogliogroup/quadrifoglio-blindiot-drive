#include <cmath>        // std::abs
#include <string>
#include <vector>
#include "ros/ros.h"
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud2.h"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
 #include <sound_play/sound_play.h>

#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e

float fSegmWdt = 0.3;
float fSegmLen = 1.0;

float lSegmWdt = 0.4;
float lSegmLen = 0.6;

float rSegmWdt = 0.4;
float rSegmLen = 0.6;

geometry_msgs::TransformStamped lidar_to_baselink;
ros::Time cmdVelTimestamp;

double map(double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
double mapConstrained(double x, double in_min, double in_max, double out_min, double out_max) {
  double temp = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  if(temp > out_max) temp = out_max;
  if(temp < out_min) temp = out_min;
  return temp;
}

void TwistTowardTwist(geometry_msgs::Twist& in, geometry_msgs::Twist& out,
    double maxLinAccX, double maxLinAccY, double maxRotAccZ/*,
    double maxLinAccXred, double maxLinAccYred, double maxRotAccZred*/){

    //if(ros::Time::now()-cmdVelTimestamp > ros::Duration(0.25))

    double secondsPassed = (ros::Time::now()-cmdVelTimestamp).toSec();  //This many seconds secondsPassed
    cmdVelTimestamp = ros::Time::now();

    double maxLinXStep = maxLinAccX * secondsPassed;  //How much the values can increase in this step
    double maxLinYStep = maxLinAccY * secondsPassed;
    double maxRotZStep = maxRotAccZ * secondsPassed;

    //If closer to the target value than step size then just set to it
    if(std::abs(in.linear.x - out.linear.x) < maxLinXStep){  
        out.linear.x = in.linear.x;
    } 
    else{  //Else increase it the step size
      if(in.linear.x > out.linear.x) out.linear.x += maxLinXStep;
      else out.linear.x -= maxLinXStep;
    }

    //Same for linear translation
    if(std::abs(in.linear.y - out.linear.y) < maxLinYStep){  
        out.linear.y = in.linear.y;
    } 
    else{  
      if(in.linear.y > out.linear.y) out.linear.y += maxLinYStep;
      else out.linear.y -= maxLinYStep;
    }

    //Same for rotation
    if(std::abs(in.angular.z - out.angular.z) < maxRotZStep){  
        out.angular.z = in.angular.z;
    } 
    else{  
      if(in.angular.z > out.angular.z) out.angular.z += maxRotZStep;
      else out.angular.z -= maxRotZStep;
    }

}

class Segment{
    public:
    Segment(visualization_msgs::MarkerArray* markerMsg, float minx, float maxx, float miny, float maxy, float r, float g, float b){
        _minx = minx; _maxx = maxx; _miny = miny; _maxy = maxy;
        _r = r; _b = b; _g = g;
        _markerMsg = markerMsg;

        _markerId = _markerIncrementer;
        _markerIncrementer++;

        visualization_msgs::Marker mark;
        mark.id = _markerId;
        mark.header.frame_id = "base_link";
        mark.header.stamp = ros::Time();
        mark.ns = "my_namespace";
        _markerMsg->markers.push_back(mark);
        _cloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _cloud->header.frame_id = "base_link";
        
        ROS_INFO("Initializing new segment with id %d", _markerId);
        
    }
    ~Segment(){
        delete _cloud;
    }
    void Clear(){
        _cloud->clear();
        _cloud->height = 1;
        _cloud->is_dense = false;
        weighted = 0;
        nearest = 1000000;
    }

    void UpdateMarker(){
        visualization_msgs::Marker marker;
        _markerMsg->markers[_markerId].header.frame_id = "base_link";
        _markerMsg->markers[_markerId].header.stamp = ros::Time();
        _markerMsg->markers[_markerId].ns = "my_namespace";
        _markerMsg->markers[_markerId].id = _markerId;
        _markerMsg->markers[_markerId].type = visualization_msgs::Marker::CUBE;
        _markerMsg->markers[_markerId].action = visualization_msgs::Marker::ADD; //Or modify!
        _markerMsg->markers[_markerId].pose.position.x = (_maxx + _minx)/2.0;
        _markerMsg->markers[_markerId].pose.position.y = (_maxy + _miny)/2.0;
        _markerMsg->markers[_markerId].pose.position.z = 0;
        _markerMsg->markers[_markerId].pose.orientation.x = 0.0;
        _markerMsg->markers[_markerId].pose.orientation.y = 0.0;
        _markerMsg->markers[_markerId].pose.orientation.z = 0.0;
        _markerMsg->markers[_markerId].pose.orientation.w = 1.0;
        _markerMsg->markers[_markerId].scale.x = std::abs(_maxx - _minx);
        _markerMsg->markers[_markerId].scale.y = std::abs(_maxy - _miny);
        _markerMsg->markers[_markerId].scale.z = 0.01;
        _markerMsg->markers[_markerId].color.a = 0.4; // Don't forget to set the alpha!
        _markerMsg->markers[_markerId].color.r = (_cloud->width) ? 1.0 : _r;
        _markerMsg->markers[_markerId].color.g = (_cloud->width) ? 0.0 : _g;
        _markerMsg->markers[_markerId].color.b = (_cloud->width) ? 0.0 : _b;
        //ROS_INFO("Publishing with ID %d", _markerId);
        //
    }

    //Fills a segment from cloud, assumes it has been cleared
    void Fill(pcl::PointCloud<pcl::PointXYZ> &inCloud){
        Clear(); //Clear the current list
        for(auto point : inCloud.points){//negative x is forward, negative y is left wrt rplidar
            if(point.x > _minx && point.x < _maxx && point.y > _miny && point.y < _maxy){
                _cloud->push_back(point);
                _cloud->width++;
                weighted++;
                float dist = std::sqrt(std::pow(point.x,2.0)+pow(point.y,2.0));
                if(dist < nearest) nearest = dist;  //record nearest point
            }
        }
        UpdateMarker();  //Update markers based on newly filled cloud
    }

    pcl::PointCloud<pcl::PointXYZ>* _cloud;
    float weighted; //"amount of points" weighted by how close they are (close points count more)
    float nearest;  //Distance of nearest point in x-y plane, very large if no points

    private:

    float _minx;  //These define the segment area
    float _maxx;
    float _miny;
    float _maxy;
    float _r;
    float _g;
    float _b;

    visualization_msgs::MarkerArray* _markerMsg;

    static int _markerIncrementer; //Incremented in constructor, saved as unique ID for marker
    int _markerId;  //The actual id for the marker.
};


class Blindiot{
    public:
    Blindiot(ros::Publisher* cloudPub, ros::Publisher* markerPub, ros::Publisher* cmdVelPub,
    visualization_msgs::MarkerArray* markerMsg, tf2_ros::TransformListener* tfListener){

        _cloudPub = cloudPub;
        _markerPub = markerPub;
        _cmdVelPub = cmdVelPub;
        _markerMsg = markerMsg;
        _tfListener = tfListener;
        _debugCloud = (new pcl::PointCloud<pcl::PointXYZ>);
        _transformCloud = (new pcl::PointCloud<pcl::PointXYZ>);

        //minx maxx miny maxy r g b
        _frontLeftSegm = new Segment(_markerMsg,0.5,1,0,0.2,   0,1,0.1);
        _frontRightSegm = new Segment(_markerMsg,0.5,1,-0.2,0,   0.1,1,0);
        _frontNearSegm = new Segment(_markerMsg,0,0.5,-0.2,0.2,   0.2,1,0.2);
        _leftSegm = new Segment(_markerMsg,-0.4,0.4,0.2,0.4,   0.2, 1,0);
        _rightSegm = new Segment(_markerMsg,-0.4,0.4,-0.4, -0.2,   0.2, 1,0);

        _lastLogicUpdated = ros::Time::now();
        _mode = 0;
        _lastMode = _mode;

        sound_play::Sound slumber = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/turnon_stirslumber.ogg");
        sound_play::Sound rot_rotationality = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/rot_rotationality.ogg");
        sound_play::Sound turnon_fromwhence = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/turnon_fromwhence.ogg");
        sound_play::Sound turnon_stirslumber = sc.waveSoundFromPkg("quadrifoglio_launch", "turnon_stirslumber.ogg");
        sound_play::Sound obstacle_cannotproceed = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/obstacle_cannotproceed.ogg");
        sound_play::Sound obstacle_trapped = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/obstacle_trapped.ogg");
        sound_play::Sound drv_on_naviprowess = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_on_naviprowess.ogg");
        sound_play::Sound drv_on_rideeternal = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_on_rideeternal.ogg");
        sound_play::Sound drv_off_trustdriving = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_off_trustdriving.ogg");
        sound_play::Sound drv_off_oaf = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_off_oaf.ogg");

        _rotateSpk.push_back(rot_rotationality);
        _turnOnSpk.push_back(turnon_fromwhence); _turnOnSpk.push_back(turnon_stirslumber);
        _obstacleSpk.push_back(obstacle_cannotproceed); _obstacleSpk.push_back(obstacle_cannotproceed);
        _drvOnSpk.push_back(drv_on_naviprowess); _drvOnSpk.push_back(drv_on_rideeternal);
        _drvOffSpk.push_back(drv_off_trustdriving);  _drvOffSpk.push_back(drv_off_oaf);

    }
    ~Blindiot(){
        delete _debugCloud;
        delete _transformCloud;

        delete _frontLeftSegm;
        delete _frontRightSegm;
        delete _frontNearSegm;
        delete _leftSegm;
        delete _rightSegm;
    }
    void outPubCb(const ros::TimerEvent&){
        //If too long time since logic update, start ramping down velocity
        if(ros::Time::now()-_lastLogicUpdated > ros::Duration(1.0)){
            _cmdVel.linear.x = _cmdVel.linear.y = _cmdVel.linear.z = 0.0;
            _cmdVel.angular.x = _cmdVel.angular.y = _cmdVel.angular.z = 0.0;
        }

        TwistTowardTwist(_cmdVel, _cmdVelOut,0.5,0.1,1.0);  //Make cmdVelOut approach cmdVel
        
        _cmdVelPub->publish(_cmdVelOut);

    }
    //const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &pclMsg
    void pclCallback(const sensor_msgs::PointCloud2ConstPtr &pclMsg){
        //Update timestamp
        _lastLogicUpdated = ros::Time::now();

        //Make empty PointCloud2
        sensor_msgs::PointCloud2 rotated;

        //Transform the incoming PointCloud2 with the laser->base_link transform
        tf2::doTransform(*pclMsg, rotated, lidar_to_baselink);

        //Convert the transform PointCloud2 to PCL
        pcl::fromROSMsg(rotated, *_transformCloud);

        //Use the transformed pcl pointcloud
        _frontLeftSegm->Fill(*_transformCloud);
        _frontRightSegm->Fill(*_transformCloud);
        _frontNearSegm->Fill(*_transformCloud);
        _rightSegm->Fill(*_transformCloud);
        _leftSegm->Fill(*_transformCloud);
        _markerPub->publish(*_markerMsg);

        _nearestNow = 9999999.0;

        for (auto pt : _transformCloud->points){
            double dist = std::sqrt(std::pow(pt.x,2.0)+pow(pt.y,2.0));
            if(dist < _nearestNow ) _nearestNow = dist;
        }

        _cmdVel.linear.y = 0.0;
        _cmdVel.linear.x = 0.1;
        _cmdVel.angular.z = 0.0;

        _cmdVel.linear.x = mapConstrained(_nearestNow,0.3,1.2,0.05,0.3);
        ROS_INFO_THROTTLE(1,"nearest %f speed %f", _nearestNow, _cmdVel.linear.x);

        //Normal mode
        if(_mode == 0){
            if(_mode != _lastMode){
                _lastMode = _mode;
                ROS_WARN("Playing normal mode sound");
                _drvOnSpk[1].play();
            }

            if(_frontNearSegm->weighted){
                _mode = 1; //Something is close to front, start in place turning
                if(_frontLeftSegm->nearest < _frontRightSegm->nearest){
                    _turnLeft = false;  //Left side has closer things, turn right
                }
                else _turnLeft = true;
            }
            else{
                if(_rightSegm->weighted) _cmdVel.linear.y += 0.1;  //Do the usual strafe
                if(_leftSegm->weighted) _cmdVel.linear.y -= 0.1; 

                //If both segments triggered, turn away from the one with closer points
                if(_frontLeftSegm->weighted && _frontRightSegm->weighted){
                    if(_frontLeftSegm->nearest < _frontRightSegm->nearest){
                        _cmdVel.angular.z = -1.0;
                    }
                    else _cmdVel.angular.z = 1.0;
                }
                else{ //If only one or neither is triggered, turn away from the one that is
                     if(_frontLeftSegm->weighted) _cmdVel.angular.z = -1.0;   
                     if(_frontRightSegm->weighted) _cmdVel.angular.z = 1.0; 
                }
            }
        }

        //In-place spin mode
        if(_mode == 1){
            if(_mode != _lastMode){
                _lastMode = _mode;
                ROS_WARN("Playing turn mode sound");
                _rotateSpk[0].play();
            }
            _cmdVel.linear.y = 0.0;
            _cmdVel.linear.x = 0.0;
            if(_turnLeft){
                _cmdVel.angular.z = 1.0;  //Turn in place
            }
            else{
                _cmdVel.angular.z = -1.0;  //Turn in place
            }
            

            //if(_rightSegm->weighted) _cmdVel.linear.y = 0.1;   //Do the usual strafe
            //if(_leftSegm->weighted) _cmdVel.linear.y = -0.1; 

            if(!_frontLeftSegm->weighted && !_frontRightSegm->weighted && !_frontNearSegm->weighted){
                _mode = 0;  //Nothing directly in front, go back to normal mode
            }
        }
    }

    private:
    ros::Publisher* _cloudPub;
    ros::Publisher* _markerPub;
    ros::Publisher* _cmdVelPub;
    pcl::PointCloud<pcl::PointXYZ>* _debugCloud;
    pcl::PointCloud<pcl::PointXYZ>* _transformCloud;
    visualization_msgs::MarkerArray* _markerMsg;
    geometry_msgs::Twist _cmdVel;  //Output from logic
    geometry_msgs::Twist _cmdVelOut;  //Actual output from node, published faster than logic output
    tf2_ros::TransformListener* _tfListener;
    ros::Time _lastLogicUpdated;  //Timestamp of last logic update, used to zero output in case of error

    Segment* _frontLeftSegm;
    Segment* _frontRightSegm;
    Segment* _frontNearSegm;
    Segment* _leftSegm;
    Segment* _rightSegm;

    sound_play::SoundClient sc;

    std::vector<sound_play::Sound> _rotateSpk;  //When rotating in place
    std::vector<sound_play::Sound> _turnOnSpk;  //When powering on
    std::vector<sound_play::Sound> _obstacleSpk;  //When center is blocked
    std::vector<sound_play::Sound> _drvOnSpk;   //When auto mode is turned on
    std::vector<sound_play::Sound> _drvOffSpk;   //When auto mode is turned off

    double _nearestNow;
    bool _turnLeft;  //Choice of turn in rotation mode
    int _mode; //mode of vurrent movement, 0= normal 1= spinsearch
    int _lastMode;  //Used to detect changes in mode
};

int Segment::_markerIncrementer = 0; //initialize the static thingy  


int main(int argc, char **argv){

    ros::init(argc, argv, "quadrifoglio_blindiot_drive");
    ros::NodeHandle n;

    cmdVelTimestamp = ros::Time::now();

    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    ROS_INFO("Attempting to get transform from lidar to base_link");
    
    try{
      lidar_to_baselink = tfBuffer.lookupTransform("lidar", "base_link", ros::Time::now(), ros::Duration(30.0));
      ROS_INFO("got the thing?");
    }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      ros::Duration(1.0).sleep();
    }

    ros::Publisher cloudPub = n.advertise<sensor_msgs::PointCloud2>("pointcloud_debug", 1);
    ros::Publisher markerPub = n.advertise<visualization_msgs::MarkerArray>("visualization_marker", 1);
    ros::Publisher cmdVelPub = n.advertise<geometry_msgs::Twist>("cmd_vel_blindiot", 1);

    visualization_msgs::MarkerArray markerMsg;
    Blindiot blindiot(&cloudPub, &markerPub, &cmdVelPub, &markerMsg, &tfListener);

    ros::Subscriber pclSub = n.subscribe<sensor_msgs::PointCloud2>("pointcloud_lidar", 1, &Blindiot::pclCallback, &blindiot);

    ros::Timer timer = n.createTimer(ros::Duration(0.0334), &Blindiot::outPubCb, &blindiot);

    //sound_play::SoundClient sc;
    //sc.play(sound_play::SoundRequest::NEEDS_PLUGGING_BADLY);
    // sound_play::Sound slumber = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/turnon_stirslumber.ogg");
    // sound_play::Sound rot_rotationality = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/rot_rotationality.ogg");
    // sound_play::Sound turnon_fromwhence = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/turnon_fromwhence.ogg");
    // sound_play::Sound turnon_stirslumber = sc.waveSoundFromPkg("quadrifoglio_launch", "turnon_stirslumber.ogg");
    // sound_play::Sound obstacle_cannotproceed = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/obstacle_cannotproceed.ogg");
    // sound_play::Sound obstacle_trapped = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/obstacle_trapped.ogg");
    // sound_play::Sound drv_on_naviprowess = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_on_naviprowess.ogg");
    // sound_play::Sound drv_on_rideeternal = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_on_rideeternal.ogg");
    // sound_play::Sound drv_off_trustdriving = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_off_trustdriving.ogg");
    // sound_play::Sound drv_off_oaf = sc.waveSoundFromPkg("quadrifoglio_launch", "sounds/drv_off_oaf.ogg");


    // std::vector<sound_play::Sound> rotateSpk;  //When rotating in place
    // std::vector<sound_play::Sound> turnOnSpk;  //When powering on
    // std::vector<sound_play::Sound> obstacleSpk;  //When center is blocked
    // std::vector<sound_play::Sound> drvOnSpk;   //When auto mode is turned on
    // std::vector<sound_play::Sound> drvOffSpk;   //When auto mode is turned off
    
    // rotateSpk.push_back(rot_rotationality);
    // turnOnSpk.push_back(turnon_fromwhence); turnOnSpk.push_back(turnon_stirslumber);
    // obstacleSpk.push_back(obstacle_cannotproceed); obstacleSpk.push_back(obstacle_cannotproceed);
    // drvOnSpk.push_back(drv_on_naviprowess); drvOnSpk.push_back(drv_on_rideeternal);
    // drvOffSpk.push_back(drv_off_trustdriving);  drvOffSpk.push_back(drv_off_oaf);


    //slumber.play();
    //turnOnSpk[0].play();
    //ros::Duration(3.0).sleep();
    //turnOnSpk[0].stop();
    //slumber.stop();

    ros::spin();

    return 0;
}
